import numpy as np
import re
import os
import shutil
from keras.preprocessing import sequence
from programcorrecter.src import TextPreprocessor as tp
import tensorflow as tf
from tensorflow.python.layers.core import Dense
from tensorflow.contrib.rnn import DropoutWrapper, MultiRNNCell


def build_cell(num_units, keep_prob):
    cell = tf.nn.rnn_cell.BasicLSTMCell(num_units)
    drop_cell = DropoutWrapper(cell, input_keep_prob=keep_prob, output_keep_prob=keep_prob)
    return drop_cell


if __name__ == '__main__':
    # Server setting
    NUM_UNITS = 300  # 10
    EMBEDDING_SIZE = 50  # 30
    BATCH_SIZE = 5  # 20
    NUM_ITERATIONS = 2  # 10
    MAX_LEN = 150  # 30

    # Server setting Embed_150
    # NUM_UNITS = 150  # 10
    # EMBEDDING_SIZE = 50  # 30
    # BATCH_SIZE = 100  # 20
    # NUM_ITERATIONS = 2  # 50
    # MAX_LEN = 150  # 30

    # Local setting
    # NUM_UNITS = 10  # 10
    # EMBEDDING_SIZE = 50  # 30
    # BATCH_SIZE = 10  # 20
    # NUM_ITERATIONS = 10  # 50
    # MAX_LEN = 150  # 30

    LEARNING_RATE = 0.001
    MAX_GRADIENT_NORM = 1
    NUM_LAYERS = 3
    KEEP_PROB = 0.8

    relative_path_parent_dir = os.path.dirname(os.path.dirname(__file__))
    # print(relative_path_parent_dir)
    filename = os.path.join(relative_path_parent_dir, 'dataset/train_pairs.txt')
    data = open(filename, 'r').read().splitlines()
    train_incorrect_programs = []
    train_correct_programs = []
    filename = os.path.join(relative_path_parent_dir, 'test/test4.txt')
    test_data = open(filename, 'r').read().splitlines()

    test_incorrect_programs = []
    test_incorrect_programs_unchanged = []
    train_sequence_lengths = []
    target_sequence_lengths = []
    test_sequence_lengths = []
    # print(len(data))
    for i in range(len(data)):
        text = data[i].split(',')
        assert len(text) == 2
        text[1] = '<sos> ' + text[1] + ' <eos>'
        text[0] = '<sos> ' + text[0] + ' <eos>'
        text[1] = str.replace(text[1], 'False', 'BOOL')
        text[0] = str.replace(text[0], 'False', 'BOOL')
        text[1] = str.replace(text[1], 'True', 'BOOL')
        text[0] = str.replace(text[0], 'True', 'BOOL')
        train_incorrect_programs.append(re.sub('\d+', 'NUM', text[1]))
        train_correct_programs.append(re.sub('\d+', 'NUM', text[0]))
        train_sequence_lengths.append(len(text[1]))
        target_sequence_lengths.append(len(text[0]))

    for i in range(len(test_data)):
        test_incorrect_programs_unchanged.append(test_data[i])
        test_data[i] = '<sos> ' + test_data[i] + ' <eos>'
        test_data[i] = str.replace(test_data[i], 'False', 'BOOL')
        test_data[i] = str.replace(test_data[i], 'True', 'BOOL')
        test_incorrect_programs.append(re.sub('\d+', 'NUM', test_data[i]))
        test_sequence_lengths.append(len(test_data[i]))

    # print(train_sequence_lengths)
    # print(target_sequence_lengths)
    # print(test_sequence_lengths)

    assert (len(train_correct_programs) == len(train_incorrect_programs))
    INPUT_SEQUENCE_COUNT = len(train_correct_programs)
    # print(INPUT_SEQUENCE_COUNT)
    print(train_correct_programs[0])
    print(train_incorrect_programs[0])
    print(test_incorrect_programs[0])

    text_processor = tp.TextProcessor()
    text_processor.train_tokenizer(train_correct_programs + train_incorrect_programs)
    x_train = text_processor.convert_text_to_sequences(train_incorrect_programs)
    x_target = text_processor.convert_text_to_sequences(train_correct_programs)
    print(x_train[0])
    # print(x_target[1])
    for i in range(len(x_train)):
        x_train[i][:] = [x - 1 for x in x_train[i]]
        x_target[i][:] = [x - 1 for x in x_target[i]]
    # print(x_train[1])
    # print(x_target[1])
    x_test = text_processor.convert_text_to_sequences(test_incorrect_programs)
    for i in range(len(x_test)):
        x_test[i][:] = [x - 1 for x in x_test[i]]
    x_output = []
    for i in range(len(x_target)):
        x_left_shifted_output = x_target[i][1:]
        x_output.append(x_left_shifted_output)
    src_vocab_size = len(text_processor.get_word_index_keys())
    # print(text_processor.get_word_index_keys())
    sos_id = text_processor.get_word_index('<sos>') - 1
    eos_id = text_processor.get_word_index('<eos>') - 1

    # print(x_train[1])
    # print(x_target[1])
    # print(x_output[1])
    # print(x_test[1])
    # print(text_processor.get_word_index_keys())
    # print(text_processor.get_word_index_values())
    # print(text_processor.get_word_index('<sos>'))
    # print(text_processor.get_word_index('<eos>'))

    x_train_seq = sequence.pad_sequences(x_train, maxlen=MAX_LEN, padding='post', truncating='post')
    print(x_train_seq[0])
    x_target_seq = sequence.pad_sequences(x_target, maxlen=MAX_LEN, padding='post', truncating='post')
    # print(x_target_seq.shape)
    x_output_seq = sequence.pad_sequences(x_output, maxlen=MAX_LEN, padding='post', truncating='post')
    # print(x_output_seq.shape)
    x_test_seq = sequence.pad_sequences(x_test, maxlen=MAX_LEN, padding='post', truncating='post')
    # print(x_test_seq.shape)

    # Inputs
    enc_inputs = tf.placeholder(tf.int32, shape=(None, None), name='enc_inputs')
    dec_inputs = tf.placeholder(tf.int32, shape=(None, None), name='dec_inputs')
    dec_outputs = tf.placeholder(tf.int32, shape=(None, None), name='dec_outputs')
    enc_seq_len = tf.placeholder(tf.int32, shape=None, name='enc_seq_len')
    dec_seq_len = tf.placeholder(tf.int32, shape=None, name='dec_seq_len')
    # # Embeddings
    embeddings_encoder = tf.get_variable("embeddings_encoder", [src_vocab_size, EMBEDDING_SIZE])
    embeddings_decoder = tf.get_variable("embeddings_decoder", [src_vocab_size, EMBEDDING_SIZE])
    encoder_embed_inputs = tf.nn.embedding_lookup(embeddings_encoder, enc_inputs, name='encoder_embed_inputs')
    decoder_embed_inputs = tf.nn.embedding_lookup(embeddings_decoder, dec_inputs, name='decoder_embed_inputs')
    # #
    encoder_cell = tf.nn.rnn_cell.BasicLSTMCell(NUM_UNITS, name='encoder_cell')
    encoder_cell = DropoutWrapper(encoder_cell, input_keep_prob=KEEP_PROB, output_keep_prob=KEEP_PROB)
    # multilayer_encoder_cell = MultiRNNCell([build_cell(NUM_UNITS, KEEP_PROB) for _ in range(NUM_LAYERS)])
    encoder_outputs, encoder_state = tf.nn.dynamic_rnn(encoder_cell, encoder_embed_inputs, dtype=tf.float32,
                                                       time_major=False, sequence_length=enc_seq_len)

    projection_layer = tf.keras.layers.Dense(src_vocab_size)
    # attention_states: [batch_size, max_time, num_units]
    attention_states = encoder_outputs
    # Create an attention mechanism
    attention_mechanism = tf.contrib.seq2seq.LuongAttention(
        NUM_UNITS, attention_states)
    # # Decoder Cell
    decoder_cell = tf.nn.rnn_cell.BasicLSTMCell(NUM_UNITS, name='decoder_cell')
    decoder_cell = DropoutWrapper(decoder_cell, input_keep_prob=KEEP_PROB, output_keep_prob=KEEP_PROB)
    decoder_cell = tf.contrib.seq2seq.AttentionWrapper(
        decoder_cell, attention_mechanism,
        attention_layer_size=NUM_UNITS)
    # multilayer_decoder_cell = MultiRNNCell([build_cell(NUM_UNITS, KEEP_PROB) for _ in range(NUM_LAYERS)])
    # # Helper
    helper = tf.contrib.seq2seq.TrainingHelper(decoder_embed_inputs, tf.constant([x_target_seq.shape[1]] * BATCH_SIZE,
                                                                                 shape=[BATCH_SIZE]), time_major=False)
    # # Decoder
    decoder_initial_state = decoder_cell.zero_state(dtype=tf.float32, batch_size=BATCH_SIZE).clone(cell_state=encoder_state)
    decoder = tf.contrib.seq2seq.BasicDecoder(decoder_cell, helper, decoder_initial_state, output_layer=projection_layer)
    # # # Dynamic decoding
    # outputs, final_state, final_sequence_lengths = tf.contrib.seq2seq.dynamic_decode(decoder)
    # logits = outputs.rnn_output
    # #
    # crossent = tf.nn.sparse_softmax_cross_entropy_with_logits(labels=dec_outputs, logits=logits)
    # train_loss = (tf.reduce_sum(crossent) / BATCH_SIZE)
    # #
    # # Calculate and clip gradients
    # params = tf.trainable_variables()
    # gradients = tf.gradients(train_loss, params)
    # clipped_gradients, _ = tf.clip_by_global_norm(gradients, MAX_GRADIENT_NORM)
    # #
    # # # Optimization
    # optimizer = tf.train.AdamOptimizer(LEARNING_RATE)
    # update_step = optimizer.apply_gradients(zip(clipped_gradients, params))
    #
    # sess = tf.Session()
    # sess.run(tf.global_variables_initializer())
    #
    # for epoch in range(NUM_ITERATIONS):
    #     loss_history = []
    #
    #     for i in range(int(INPUT_SEQUENCE_COUNT/BATCH_SIZE)):  # Change BATCH_SIZE to int(INPUT_SEQUENCE_COUNT/BATCH_SIZE)
    #         _, loss = sess.run((update_step, train_loss),
    #                         {enc_inputs: x_train_seq[i * BATCH_SIZE:(i + 1) * BATCH_SIZE, :],
    #                          enc_seq_len: train_sequence_lengths[i * BATCH_SIZE:(i + 1) * BATCH_SIZE],
    #                          dec_inputs: x_target_seq[i * BATCH_SIZE:(i + 1) * BATCH_SIZE, :],
    #                          dec_seq_len: target_sequence_lengths[i * BATCH_SIZE:(i + 1) * BATCH_SIZE],
    #                          dec_outputs: x_output_seq[i * BATCH_SIZE:(i + 1) * BATCH_SIZE, :]})
    #
    #         loss_history.append(loss)
    #
    #     if (epoch + 1) % 2 == 0:
    #         print('Epoch: ' + str(epoch + 1), ' Loss: ' + str(np.mean(loss_history)))
    #
    # # Save model
    # if os.path.isdir(os.path.join(relative_path_parent_dir, 'saved_model_data')):
    #     shutil.rmtree(os.path.join(relative_path_parent_dir, 'saved_model_data'))
    # export_dir = os.path.join(relative_path_parent_dir, 'saved_model_data')
    # saver = tf.train.Saver()
    # saver.save(sess=sess, save_path=export_dir + '/session', write_meta_graph=False)
    # builder = tf.saved_model.builder.SavedModelBuilder(export_dir)
    # builder.add_meta_graph_and_variables(sess,
    #                                      ['train_model'])
    # builder.save()

    # # Test Output
    maximum_iterations = tf.round(tf.reduce_max(x_test_seq.shape[1]) * 2)
    helper_test = tf.contrib.seq2seq.GreedyEmbeddingHelper(embeddings_decoder, tf.fill([BATCH_SIZE], sos_id), eos_id)

    # Decoder
    decoder_test = tf.contrib.seq2seq.BasicDecoder(decoder_cell, helper_test, decoder_initial_state, output_layer=projection_layer)
    # Dynamic decoding
    outputs_test, _, _ = tf.contrib.seq2seq.dynamic_decode(decoder_test, maximum_iterations=maximum_iterations)
    translations = outputs_test.sample_id

    sess = tf.Session()
    # export_dir = os.path.join(relative_path_parent_dir, 'saved_model_data')
    export_dir = os.path.join(relative_path_parent_dir, 'saved_models/iter_10/saved_model_data')
    # tf.saved_model.loader.load(sess, ['train_model'], export_dir)
    tf.train.Saver().restore(sess, export_dir + '/session')

    batch_test_output = []
    for i in range(int(x_test_seq.shape[0]/BATCH_SIZE)):  # Change BATCH_SIZE to int(x_test_seq.shape[0]/BATCH_SIZE)
        outputs_test_res = sess.run(outputs_test, {enc_inputs: x_test_seq[i * BATCH_SIZE:(i + 1) * BATCH_SIZE, :],
                                                   enc_seq_len: test_sequence_lengths[i * BATCH_SIZE:(i + 1) * BATCH_SIZE]})
        # print(outputs_test_res)
        # print(outputs_test_res.sample_id)
        batch_test_output.append(outputs_test_res.sample_id)

    if not os.path.exists(os.path.join(relative_path_parent_dir, "output_data")):
        os.makedirs(os.path.join(relative_path_parent_dir, "output_data"))
    if os.path.exists(os.path.join(relative_path_parent_dir, 'output_data/test_output.txt')):
        os.remove(os.path.join(relative_path_parent_dir, 'output_data/test_output.txt'))
    output_file = open(os.path.join(relative_path_parent_dir, 'output_data/test_output_4.txt'), 'a')
    key_list = list(text_processor.get_word_index_keys())
    # print(key_list)
    # print(key_list[0])
    for i in range(len(batch_test_output)):
        mini_batch_outputs = batch_test_output[i]
        for j in range(len(mini_batch_outputs)):
            output_program_id = mini_batch_outputs[j]
            output_text = []
            for k in range(len(output_program_id)):
                if output_program_id[k] == eos_id:
                    break
                program_str = key_list[output_program_id[k]]
                output_text.append(program_str)
                output_file.write(program_str + ' ')
            # print(output_text)
            output_file.write('\n')
    output_file.close()

    if os.path.exists(os.path.join(relative_path_parent_dir, 'output_data/valid_output.txt')):
        os.remove(os.path.join(relative_path_parent_dir, 'output_data/valid_output.txt'))
    final_output_file = open(os.path.join(relative_path_parent_dir, 'output_data/valid_output_4.txt'), 'a')
    filename = os.path.join(relative_path_parent_dir, 'output_data/test_output_4.txt')
    output_data = open(filename, 'r').read().splitlines()
    print(len(output_data))
    for i in range(len(output_data)):
        digits_occurrences = re.findall('\d+', test_incorrect_programs_unchanged[i])
        nums_occurrences = re.findall('NUM', output_data[i])
        # print(digits)
        # print(nums)
        min_digit_count = np.min([len(nums_occurrences), len(digits_occurrences)])
        for j in range(min_digit_count):
            output_data[i] = output_data[i].replace('NUM', digits_occurrences[j], 1)
        # print(output_data[i])

        true_false_occurrences = re.findall('True|False', test_incorrect_programs_unchanged[i])
        bool_occurrences = re.findall('BOOL', output_data[i])
        min_bool_count = np.min([len(true_false_occurrences), len(bool_occurrences)])
        for j in range(min_bool_count):
            output_data[i] = output_data[i].replace('BOOL', true_false_occurrences[j], 1)

        print(output_data[i])
        final_output_file.write(output_data[i])
        final_output_file.write('\n')

    final_output_file.close()



