import numpy as np
import csv
from keras.preprocessing.text import text_to_word_sequence
from keras.preprocessing.text import Tokenizer
from programcorrecter.src import Tokenization


class TextProcessor:

    def __init__(self):
        self._tokenizer = Tokenizer(filters='!"#$%&./:?@[\\]`~\t\n', lower=False)

    def train_tokenizer(self, data):
        self._tokenizer.fit_on_texts(data)

    @staticmethod
    def process_csv(filename):
        data = []
        with open(filename, newline='') as csvfile:
            reader = csv.reader(csvfile)
            for row in reader:
                data.append(row)
        train_data = np.array(data).reshape(len(data))
        # Remove Pos and Neg words from every row
        text = ''
        for i in range(len(data)):
            train_data[i] = train_data[i][3:]
            stemmed_sent = ' '.join(text_to_word_sequence(' '.join(Tokenization.stem_tokens(train_data[i]))))
            filtered_sentence, stop_words = Tokenization.tokenize_words_without_stopwords(stemmed_sent)
            train_data[i] = ' '.join(filtered_sentence)
            text += train_data[i]
        return train_data

    @staticmethod
    def process_text(filename):
        data = open(filename, 'r').read().replace('\n', '')
        entire_text = np.array(data).reshape(1)

        for i in range(len(entire_text)):
            stemmed_sent = ' '.join(text_to_word_sequence(' '.join(Tokenization.stem_tokens(entire_text[i]))))
            filtered_sentence, stop_words = Tokenization.tokenize_words_without_stopwords(stemmed_sent)
            entire_text[i] = ' '.join(filtered_sentence)
        return entire_text

    def convert_text_to_matrix(self, data, mode="binary"):
        one_hot_encoded_docs = self._tokenizer.texts_to_matrix(data, mode=mode)
        return one_hot_encoded_docs

    def convert_text_to_sequences(self, texts):
        sequences = self._tokenizer.texts_to_sequences(texts)
        return sequences

    def get_word_index(self, word):
        index = self._tokenizer.word_index.get(word)
        return index

    def get_word_index_keys(self):
        return self._tokenizer.word_index.keys()

    def get_word_index_values(self):
        return self._tokenizer.word_index.values()








