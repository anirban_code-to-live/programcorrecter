# -----------------------------------------------------------------------------
# A simple parser for Assignment 2.
# -----------------------------------------------------------------------------

import os, sys

os.system("pip2 install --user ply")
os.system("pip2 install --user editdistance")

tokens = (
    'VAR','NUMBER',
    'PLUS','MINUS','TIMES','EQUALS','LESSEQ',
    'LPAREN','RPAREN',
    'AND','NOT','TRUE','FALSE','IF','ELSE','WHILE',
    'SEMICOL','LCURLY','RCURLY'
    )

# Tokens

t_PLUS    = r'\+'
t_MINUS   = r'-'
t_TIMES   = r'\*'
t_LESSEQ  = r'<='
t_EQUALS  = r'='
t_AND     = r'\^'
t_NOT     = r'\~'
t_TRUE    = r'True'
t_FALSE   = r'False'
t_IF      = r'if'
t_ELSE    = r'else'
t_WHILE   = r'while'
t_SEMICOL = r';'
t_LPAREN  = r'\('
t_RPAREN  = r'\)'
t_LCURLY  = r'\{'
t_RCURLY  = r'\}'
t_VAR     = r'x'

def t_NUMBER(t):
    r'\d+'
    t.value = int(t.value)
    return t

# Ignored characters
t_ignore = " \t"

def t_newline(t):
    r'\n+'
    t.lexer.lineno += t.value.count("\n")

def t_error(t):
    print("Illegal character '%s'" % t.value[0])
    t.lexer.skip(1)

# Build the lexer
import ply.lex as lex
lex.lex()

# Precedence rules for the arithmetic operators
precedence = (
    ('left', 'NOT'),
    ('left', 'AND'),
    ('left', 'EQUALS'),
    ('left', 'LESSEQ'),
    ('left','PLUS','MINUS'),
    ('left','TIMES'),
    )

# dictionary of names (for storing variables)
names = { }

def p_statement_continue(p):
    'statement : statement SEMICOL statement'
    pass

def p_statement_if(p):
    'statement : IF LPAREN boolexpr RPAREN LCURLY statement RCURLY ELSE LCURLY statement RCURLY'
    pass

def p_statement_while(p):
    'statement : WHILE LPAREN boolexpr RPAREN LCURLY statement RCURLY'
    pass

def p_statement_assign(p):
    'statement : VAR EQUALS expression'
    pass

def p_expression_binop(p):
    '''expression : expression PLUS expression
                  | expression MINUS expression
                  | expression TIMES expression'''
    pass

def p_expression_number(p):
    'expression : NUMBER'
    pass

def p_expression_variable(p):
    'expression : VAR'
    pass

def p_boolexpr_literal(p):
    '''boolexpr : TRUE
                | FALSE'''
    pass

def p_boolexpr_binop(p):
    '''boolexpr : expression EQUALS expression
                | expression LESSEQ expression
                | boolexpr AND boolexpr'''
    pass

def p_boolexpr_not(p):
    '''boolexpr : NOT boolexpr'''
    pass

class SyntaxErrorFound(Exception):
    pass

def p_error(p):
    raise SyntaxErrorFound

import ply.yacc as yacc
yacc.yacc()

## BATCH MODE ##
if len(sys.argv) == 3:
    file1 = sys.argv[1]
    file2 = sys.argv[2]
elif len(sys.argv) == 1:
    file1 = 'valid_input.txt'
    file2 = 'sample_output.txt'
else:
    print '''Accepts 0 or 2 arguments, usage:\nparser.py [input_filename output_filename]'''
    sys.exit(0)

print file1, 'used as input.'
print file2, 'used as output.', '\n'

import os.path

if not os.path.isfile(file1):
    print file1, 'is not a valid file path'
    sys.exit(0)
if not os.path.isfile(file2):
    print file2, 'is not a valid file path'
    sys.exit(0)

with open('valid_input.txt', 'r') as f:
    input_lines = f.readlines()    

with open('sample_output.txt', 'r') as f:
    output_lines = f.readlines()    

input_lines  = [s.strip() for s in input_lines]
output_lines = [s.strip() for s in output_lines]

repaired_history = []
results = []
edit_distances = []

import editdistance

for in_line, out_line in zip(input_lines, output_lines):
    repaired = True

    try:
        yacc.parse(out_line)
    except SyntaxErrorFound:
        repaired = False
    
    dist = editdistance.eval(in_line.replace(' ', ''), out_line.replace(' ', ''))
    
    edit_distances += [dist]
    repaired_history += [repaired]
    results += [repaired and dist <= 1]

import numpy as np

print 'Repair percentage: %.2f%%' % (np.mean(repaired_history) * 100), '(%d of %d)' % (np.count_nonzero(repaired_history), len(repaired_history)), '\n'

print 'Average edit distance: %.2f' % (np.mean(edit_distances))
print 'Minimum edit distance: %.2f' % (np.min(edit_distances))
print 'Maximum edit distance: %.2f' % (np.max(edit_distances)), '\n'

print 'Adjusted result: %.2f' % (np.mean(results) * 100), '(%d of %d)' % (np.count_nonzero(results), len(results))
print 'The adjusted result only accounts for single edit fixes', '\n'

